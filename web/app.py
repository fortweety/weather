import requests
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from datetime import datetime


app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
api = Api(app)
LON = 82.9346
LAT = 55.0415
UNITS = "metric"
APPID = "55d0dab505894eaa8fc0c4e59856e812"


class Weather(Resource):
    def get(self):
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                         params={'lon': LON, 'lat': LAT, "units": UNITS, 'APPID': APPID})
        data = res.json()

        if res.status_code != 200:
            resJson = {
                "status": data["cod"],
                "error": data["message"]
            }
            return jsonify(resJson)

        resJson = {
            "location": {
                "lon": data["coord"]["lon"],
                "lat": data["coord"]["lat"]
            },
            "title": data["name"],
            "timestamp": datetime.utcfromtimestamp(data["dt"]).strftime('%Y-%m-%dT%H:%M:%S.%f'),
            "temperature": {
                "C": data["main"]["temp"]
            },
            "humidity": {
                "percent": data["main"]["humidity"]
            },
            "pressure": {
                "atm": data["main"]["pressure"]
            }
        }

        return jsonify(resJson)

api.add_resource(Weather, '/weather/current/')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
